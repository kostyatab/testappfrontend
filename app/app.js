///<reference path="components/angular/angular.d.ts"/>
///<reference path="controllers/newsCtrl.ts"/>
///<reference path="controllers/loginCtrl.ts"/>
///<reference path="controllers/registrationCtrl.ts"/>
///<reference path="controllers/staticticCtrl.ts"/>
///<reference path="controllers/accountCtrl.ts"/>
///<reference path="controllers/newsItemCtrl.ts"/>
///<reference path="controllers/createNewsCtrl.ts"/>
///<reference path="service/TestAppRepository.ts"/>
///<reference path="service/AccoutManager.ts"/>
///<reference path="service/appConfig.ts"/>
'use strict';
var testApp;
(function (testApp) {
    var myApp = angular.module('myApp', ['ui.bootstrap', 'ngRoute'])
        .controller('newsCtrl', testApp.newsCtrl)
        .controller('registrationCtrl', testApp.registrationCtrl)
        .controller('loginCtrl', testApp.loginCtrl)
        .controller('staticticCtrl', testApp.staticticCtrl)
        .controller('newsItemCtrl', testApp.newsItemCtrl)
        .controller('accountCtrl', testApp.accountCtrl)
        .controller('createNewsCtrl', testApp.createNewsCtrl)
        .service('repository', testApp.TestAppRepository)
        .service('accoutManager', testApp.AccoutManager)
        .service('appConfig', testApp.appConfig)
        .config(function ($routeProvider) {
        $routeProvider.when('/news-list', {
            templateUrl: 'views/news-list.html',
            controller: 'newsCtrl'
        });
        $routeProvider.when('/news/:id', {
            templateUrl: 'views/news-item.html',
            controller: 'newsItemCtrl'
        });
        $routeProvider.when('/news-create', {
            templateUrl: 'views/createNews.html',
            controller: 'createNewsCtrl'
        });
        $routeProvider.when('/registration', {
            templateUrl: 'views/registration.html',
            controller: 'registrationCtrl'
        });
        $routeProvider.when('/login', {
            templateUrl: 'views/login.html',
            controller: 'loginCtrl'
        });
        $routeProvider.otherwise({ redirectTo: '/news-list' });
    })
        .run(function (accoutManager) {
        accoutManager.getUserInfo();
    });
})(testApp || (testApp = {}));
//# sourceMappingURL=app.js.map