///<reference path="components/angular/angular.d.ts"/>
///<reference path="controllers/newsCtrl.ts"/>
///<reference path="controllers/loginCtrl.ts"/>
///<reference path="controllers/registrationCtrl.ts"/>
///<reference path="controllers/staticticCtrl.ts"/>
///<reference path="controllers/accountCtrl.ts"/>
///<reference path="controllers/newsItemCtrl.ts"/>
///<reference path="controllers/createNewsCtrl.ts"/>
///<reference path="service/TestAppRepository.ts"/>
///<reference path="service/AccoutManager.ts"/>
///<reference path="service/appConfig.ts"/>


'use strict';

namespace testApp{
  let myApp = angular.module('myApp', ['ui.bootstrap', 'ngRoute'])
    .controller('newsCtrl', newsCtrl)
    .controller('registrationCtrl', registrationCtrl)
    .controller('loginCtrl', loginCtrl)
    .controller('staticticCtrl', staticticCtrl)
    .controller('newsItemCtrl', newsItemCtrl)
    .controller('accountCtrl', accountCtrl)
    .controller('createNewsCtrl', createNewsCtrl)
    .service('repository', TestAppRepository)
    .service('accoutManager', AccoutManager)
    .service('appConfig', appConfig)
    .config($routeProvider => {
      $routeProvider.when('/news-list',
        {
          templateUrl:'views/news-list.html',
          controller:'newsCtrl'
        });
      $routeProvider.when('/news/:id',
        {
          templateUrl:'views/news-item.html',
          controller:'newsItemCtrl'
        });
      $routeProvider.when('/news-create',
        {
          templateUrl:'views/createNews.html',
          controller:'createNewsCtrl'
        });

      $routeProvider.when('/registration',
        {
          templateUrl:'views/registration.html',
          controller:'registrationCtrl'
        });
      $routeProvider.when('/login',
        {
          templateUrl:'views/login.html',
          controller:'loginCtrl'
        });

      $routeProvider.otherwise({redirectTo: '/news-list'})
    })
    .run((accoutManager)=>{
      accoutManager.getUserInfo();
    });
}