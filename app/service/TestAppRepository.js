///<reference path="../abstract/ITestAppRepository.ts"/>
///<reference path="../abstract/INews.ts"/>
var testApp;
(function (testApp) {
    var TestAppRepository = (function () {
        function TestAppRepository($http, $q, accoutManager, appConfig) {
            this._$http = $http;
            this._$q = $q;
            this._accoutManager = accoutManager;
            this._appConfig = appConfig;
        }
        TestAppRepository.prototype.getNewsList = function () {
            var deferred = this._$q.defer();
            var than = this;
            this._$http({
                method: 'GET',
                url: than._appConfig.urlBackend.toString() + 'api/news',
                headers: { "Authorization": "Bearer " + this._accoutManager.getToken() }
            }).
                success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).
                error(function (data, status, headers, config) {
                deferred.reject(status);
            });
            return deferred.promise;
        };
        TestAppRepository.prototype.getNewsItem = function (id) {
            var deferred = this._$q.defer();
            var than = this;
            this._$http({
                method: 'GET',
                url: than._appConfig.urlBackend.toString() + 'api/news/' + id,
                headers: { "Authorization": "Bearer " + this._accoutManager.getToken() }
            }).
                success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).
                error(function (data, status, headers, config) {
                deferred.reject(status);
            });
            return deferred.promise;
        };
        TestAppRepository.prototype.getStatistic = function () {
            var deferred = this._$q.defer();
            var than = this;
            this._$http({
                method: 'GET',
                url: than._appConfig.urlBackend.toString() + 'api/statictic',
                headers: { "Authorization": "Bearer " + this._accoutManager.getToken(), 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
            }).
                success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).
                error(function (data, status, headers, config) {
                deferred.reject(status);
            });
            return deferred.promise;
        };
        TestAppRepository.prototype.createNews = function (news) {
            var deferred = this._$q.defer();
            var than = this;
            this._$http({
                method: 'POST',
                url: than._appConfig.urlBackend.toString() + 'api/news/create',
                headers: { "Authorization": "Bearer " + this._accoutManager.getToken() },
                data: news
            }).
                success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).
                error(function (data, status, headers, config) {
                deferred.reject(status);
            });
            return deferred.promise;
        };
        return TestAppRepository;
    }());
    testApp.TestAppRepository = TestAppRepository;
})(testApp || (testApp = {}));
//# sourceMappingURL=TestAppRepository.js.map