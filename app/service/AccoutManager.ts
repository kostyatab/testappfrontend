///<reference path="../abstract/IUserInfo.ts"/>

namespace testApp{
  export class AccoutManager{
    private _$http;
    private _$q;
    private _$rootScope;
    private _appConfig;

    constructor($http, $q, $rootScope, appConfig){
      this._$http = $http;
      this._$q = $q;
      this._$rootScope = $rootScope;
      this._appConfig = appConfig;
    }
    
    registration(username: string, password: string){
      let deferred = this._$q.defer();
      let than = this;

      this._$http({
        method: 'POST',
        url: than._appConfig.urlBackend.toString() + 'api/Account/Register',
        data: {Email: username, Password: password, ConfirmPassword: password}
      }).
      success(function(data, status, headers, config) {
        deferred.resolve(data);
      }).
      error(function(data, status, headers, config) {
        deferred.reject(status);
      });

      return deferred.promise;
    }

    login(username: string, password: string){
      let deferred = this._$q.defer();
      let than = this;

      var transform = function(data){
        return $.param(data);
      };

      this._$http({
        method: 'POST',
        url: than._appConfig.urlBackend.toString() + 'Token',
        data: {grant_type: 'password', username: username, password: password},
        headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
        transformRequest: transform
      }).
      success(function(data, status, headers, config) {
        deferred.resolve(data);
      }).
      error(function(data, status, headers, config) {
        deferred.reject(status);
      });

      return deferred.promise;
    }

    getToken(){
      return sessionStorage.getItem('tokenKey');
    }

    getUserInfo(){
      let temp = this._$rootScope;
      let than = this;

      this._$http({
        method: 'GET',
        url: than._appConfig.urlBackend.toString() + 'api/UserInfo',
        headers: {"Authorization" : "Bearer " + this.getToken()}
      }).
      success(function(data:IUserInfo, status, headers, config) {
        temp.userInfo = data;
      }).
      error(function(data, status, headers, config) {
      });
    }

    logout(){
      this._$rootScope.userInfo.IsAuthorized = false;
      this._$rootScope.userInfo.Name = null;
      this._$rootScope.userInfo.Role = null;

      sessionStorage.removeItem('tokenKey');
    }
    
  }
}