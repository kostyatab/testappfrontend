///<reference path="../abstract/IUserInfo.ts"/>
var testApp;
(function (testApp) {
    var AccoutManager = (function () {
        function AccoutManager($http, $q, $rootScope, appConfig) {
            this._$http = $http;
            this._$q = $q;
            this._$rootScope = $rootScope;
            this._appConfig = appConfig;
        }
        AccoutManager.prototype.registration = function (username, password) {
            var deferred = this._$q.defer();
            var than = this;
            this._$http({
                method: 'POST',
                url: than._appConfig.urlBackend.toString() + 'api/Account/Register',
                data: { Email: username, Password: password, ConfirmPassword: password }
            }).
                success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).
                error(function (data, status, headers, config) {
                deferred.reject(status);
            });
            return deferred.promise;
        };
        AccoutManager.prototype.login = function (username, password) {
            var deferred = this._$q.defer();
            var than = this;
            var transform = function (data) {
                return $.param(data);
            };
            this._$http({
                method: 'POST',
                url: than._appConfig.urlBackend.toString() + 'Token',
                data: { grant_type: 'password', username: username, password: password },
                headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
                transformRequest: transform
            }).
                success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).
                error(function (data, status, headers, config) {
                deferred.reject(status);
            });
            return deferred.promise;
        };
        AccoutManager.prototype.getToken = function () {
            return sessionStorage.getItem('tokenKey');
        };
        AccoutManager.prototype.getUserInfo = function () {
            var temp = this._$rootScope;
            var than = this;
            this._$http({
                method: 'GET',
                url: than._appConfig.urlBackend.toString() + 'api/UserInfo',
                headers: { "Authorization": "Bearer " + this.getToken() }
            }).
                success(function (data, status, headers, config) {
                temp.userInfo = data;
            }).
                error(function (data, status, headers, config) {
            });
        };
        AccoutManager.prototype.logout = function () {
            this._$rootScope.userInfo.IsAuthorized = false;
            this._$rootScope.userInfo.Name = null;
            this._$rootScope.userInfo.Role = null;
            sessionStorage.removeItem('tokenKey');
        };
        return AccoutManager;
    }());
    testApp.AccoutManager = AccoutManager;
})(testApp || (testApp = {}));
//# sourceMappingURL=AccoutManager.js.map