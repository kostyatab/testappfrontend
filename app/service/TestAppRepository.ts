///<reference path="../abstract/ITestAppRepository.ts"/>
///<reference path="../abstract/INews.ts"/>



namespace testApp{
  
  export class TestAppRepository implements ITestAppRepository{
    private _$http;
    private _$q;
    private _accoutManager;
    private _appConfig;

    constructor($http, $q, accoutManager, appConfig){
      this._$http = $http;
      this._$q = $q;
      this._accoutManager = accoutManager;
      this._appConfig = appConfig;
    }

    getNewsList() {
      let deferred = this._$q.defer();
      let than = this;

      this._$http({
        method: 'GET',
        url: than._appConfig.urlBackend.toString() + 'api/news',
        headers: {"Authorization" : "Bearer " + this._accoutManager.getToken()}
      }).
      success(function(data, status, headers, config) {
        deferred.resolve(data);
      }).
      error(function(data, status, headers, config) {
        deferred.reject(status);
      });

      return deferred.promise;
    }

    getNewsItem(id: number) {
      let deferred = this._$q.defer();
      let than = this;

      this._$http({
        method: 'GET',
        url: than._appConfig.urlBackend.toString() + 'api/news/'+id,
        headers: {"Authorization" : "Bearer " + this._accoutManager.getToken()}
      }).
      success(function(data, status, headers, config) {
        deferred.resolve(data);
      }).
      error(function(data, status, headers, config) {
        deferred.reject(status);
      });

      return deferred.promise;
    }

    getStatistic(){
      let deferred = this._$q.defer();
      let than = this;

      this._$http({
        method: 'GET',
        url: than._appConfig.urlBackend.toString() + 'api/statictic',
        headers: {"Authorization" : "Bearer " + this._accoutManager.getToken(), 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
      }).
      success(function(data, status, headers, config) {
        deferred.resolve(data);
      }).
      error(function(data, status, headers, config) {
        deferred.reject(status);
      });
      
      return deferred.promise;
    }

    createNews(news: INews){
      let deferred = this._$q.defer();
      let than = this;

      this._$http({
        method: 'POST',
        url: than._appConfig.urlBackend.toString() + 'api/news/create',
        headers: {"Authorization" : "Bearer " + this._accoutManager.getToken()},
        data: news
      }).
      success(function(data, status, headers, config) {
        deferred.resolve(data);
      }).
      error(function(data, status, headers, config) {
        deferred.reject(status);
      });

      return deferred.promise;
    }
  }
}