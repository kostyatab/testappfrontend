namespace testApp{
  export interface IUserInfo{
    IsAuthorized: boolean;
    Name: string;
    Role: string;
  }
}