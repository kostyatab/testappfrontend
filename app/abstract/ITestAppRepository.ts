///<reference path="../abstract/INews.ts"/>


namespace testApp{
  
  export interface ITestAppRepository{
    getNewsList();
    getNewsItem(id: number);
    getStatistic();
    createNews(news: INews);
  }
}


