namespace testApp{
  export interface INews{
    Id:number;
    Title: string
    Content: string
    Date: Date
    Autor: string
  }
}