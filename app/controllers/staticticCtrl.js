var testApp;
(function (testApp) {
    var staticticCtrl = (function () {
        function staticticCtrl($scope, repository) {
            this._$scope = $scope;
            var promiseObj = repository.getStatistic();
            promiseObj.then(function (value) {
                this.statistics = value;
                $scope.statistics = this.statistics;
            });
        }
        return staticticCtrl;
    }());
    testApp.staticticCtrl = staticticCtrl;
})(testApp || (testApp = {}));
//# sourceMappingURL=staticticCtrl.js.map