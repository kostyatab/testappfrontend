///<reference path="../service/TestAppRepository.ts"/>
var testApp;
(function (testApp) {
    var registrationCtrl = (function () {
        function registrationCtrl($scope, accoutManager) {
            this._$scope = $scope;
            this._accoutManager = accoutManager;
            $scope.vm = this;
            $scope.email = '';
            $scope.password = '';
            $scope.messageVisible = false;
            $scope.formVisible = true;
        }
        registrationCtrl.prototype.submit = function () {
            var promiseObj = this._accoutManager.registration(this._$scope.email, this._$scope.password);
            var temp = this._$scope;
            promiseObj.then(function (value) {
                temp.messageVisible = true;
                temp.formVisible = false;
            });
        };
        return registrationCtrl;
    }());
    testApp.registrationCtrl = registrationCtrl;
})(testApp || (testApp = {}));
//# sourceMappingURL=registrationCtrl.js.map