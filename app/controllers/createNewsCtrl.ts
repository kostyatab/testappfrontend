///<reference path="../abstract/ITestAppRepository.ts"/>

namespace testApp{
  
  export class createNewsCtrl{
    _$scope: any;
    _repository: ITestAppRepository;
    
    constructor($scope, repository: ITestAppRepository){
      this._$scope = $scope;
      this._repository = repository;
      $scope.vm = this;
      $scope.visibleForm = true;
      $scope.visibleText = false;
    }
    
    submit(){
      let news: INews = {
        Id: 0,
        Autor: this._$scope.Autor,
        Title: this._$scope.Title,
        Content: this._$scope.Content,
        Date: new Date()
      };

      let than = this;

      let promiseObj = this._repository.createNews(news);

      promiseObj.then(function(value) {
        than._$scope.visibleForm = false;
        than._$scope.visibleText = true;
      });
    }
  }
}