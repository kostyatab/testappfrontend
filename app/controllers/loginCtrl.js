var testApp;
(function (testApp) {
    var loginCtrl = (function () {
        function loginCtrl($scope, accoutManager, $rootScope) {
            this._$scope = $scope;
            this._accoutManager = accoutManager;
            this._$rootScope = $rootScope;
            $scope.vm = this;
            $scope.email = '';
            $scope.password = '';
            $scope.messageVisible = false;
            $scope.formVisible = true;
        }
        loginCtrl.prototype.submit = function () {
            var promiseObj = this._accoutManager.login(this._$scope.email, this._$scope.password);
            var temp = this;
            promiseObj.then(function (value) {
                temp._$rootScope.userInfo.IsAuthorized = true;
                temp._$rootScope.userInfo.Name = value.userName;
                temp._$rootScope.userInfo.Role = value.Role;
                sessionStorage.setItem('tokenKey', value.access_token);
                temp._$scope.messageVisible = true;
                temp._$scope.formVisible = false;
            });
        };
        return loginCtrl;
    }());
    testApp.loginCtrl = loginCtrl;
})(testApp || (testApp = {}));
//# sourceMappingURL=loginCtrl.js.map