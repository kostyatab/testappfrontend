namespace testApp{
  export class accountCtrl{
    private _$scope:any;
    private _accoutManager:any;
    private _$rootScope:any;

    constructor($scope, accoutManager, $rootScope){
      this._$scope = $scope;
      $scope.vm = this;
      this._accoutManager = accoutManager;

      $rootScope.$watch('userInfo', ()=>{$scope.userInfo = $rootScope.userInfo;});

    }
    
    logout(){
      this._accoutManager.logout();
    }
  }
}
