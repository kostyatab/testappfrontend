
namespace testApp{
  export class loginCtrl{
    private _$scope;
    private _accoutManager;
    private _$rootScope;

    constructor($scope, accoutManager, $rootScope){
      this._$scope = $scope;
      this._accoutManager = accoutManager;
      this._$rootScope = $rootScope;
      $scope.vm = this;

      $scope.email = '';
      $scope.password = '';

      $scope.messageVisible = false;
      $scope.formVisible = true;
    }

    submit(){
      let promiseObj = this._accoutManager.login(this._$scope.email, this._$scope.password);
      let temp = this;

      promiseObj.then(function(value) {
        temp._$rootScope.userInfo.IsAuthorized = true;
        temp._$rootScope.userInfo.Name = value.userName;
        temp._$rootScope.userInfo.Role = value.Role;
        sessionStorage.setItem('tokenKey', value.access_token);
        temp._$scope.messageVisible = true;
        temp._$scope.formVisible = false;
      });
    }
  }
}