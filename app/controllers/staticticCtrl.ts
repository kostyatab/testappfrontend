namespace testApp{
  export class staticticCtrl{
    private _$scope;
    private statistics;
    
    constructor($scope, repository){
      this._$scope = $scope;

      let promiseObj = repository.getStatistic();
      promiseObj.then(function(value) {
        this.statistics = value;
        $scope.statistics = this.statistics;
      });
    }
  }
}