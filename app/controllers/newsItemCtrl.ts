namespace testApp{
  export class newsItemCtrl{
    private _$scope;
    
    constructor($scope, repository, $routeParams){
      this._$scope = $scope;

      var id = $routeParams["id"];
      if(id!=='undefined'){
        let promiseObj = repository.getNewsItem(id);

        promiseObj.then(function(value) {
          $scope.news = value;
        });
      }
    }
  }
}