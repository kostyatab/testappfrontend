var testApp;
(function (testApp) {
    var newsCtrl = (function () {
        function newsCtrl($scope, repository, $rootScope) {
            this._$scope = $scope;
            this._$rootScope = $rootScope;
            $scope.vm = this;
            $rootScope.$watch('userInfo', function () { $scope.userInfo = $rootScope.userInfo; });
            var promiseObj = repository.getNewsList();
            promiseObj.then(function (value) {
                $scope._list = value;
                $scope.listViewModel = {};
                $scope.listViewModel.IsAuthorized = $scope._list.IsAuthorized;
                $scope.listViewModel.News = $scope._list.News.slice();
            });
            $scope.content = '';
            $scope.autor = '';
            $scope.dateOptions = {
                formatYear: 'yy',
                maxDate: new Date(2020, 5, 22),
                minDate: new Date(2000, 1, 1),
                startingDay: 1
            };
            $scope.popup1 = {
                opened: false
            };
            $scope.popup2 = {
                opened: false
            };
            $scope.dt1 = '';
            $scope.dt2 = '';
            $scope.isRead;
        }
        newsCtrl.prototype.open1 = function () {
            this._$scope.popup1.opened = true;
        };
        newsCtrl.prototype.open2 = function () {
            this._$scope.popup2.opened = true;
        };
        newsCtrl.prototype.dateRange = function () {
            var _this = this;
            if (this._$scope.dt1.toString().trim() !== '' && this._$scope.dt2.toString().trim() !== '') {
                this._$scope.dt1 = new Date(this._$scope.dt1);
                this._$scope.dt2 = (new Date(this._$scope.dt2)).setHours(23);
                console.log(this._$scope._list);
                console.log(this._$scope.listViewModel);
                var viewList = this._$scope._list.News.filter(function (x) { return (new Date(x.Date) >= _this._$scope.dt1) && (new Date(x.Date) <= _this._$scope.dt2); });
                this._$scope.listViewModel.News = viewList;
            }
        };
        return newsCtrl;
    }());
    testApp.newsCtrl = newsCtrl;
})(testApp || (testApp = {}));
//# sourceMappingURL=newsCtrl.js.map