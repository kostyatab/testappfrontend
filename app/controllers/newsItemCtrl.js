var testApp;
(function (testApp) {
    var newsItemCtrl = (function () {
        function newsItemCtrl($scope, repository, $routeParams) {
            this._$scope = $scope;
            var id = $routeParams["id"];
            if (id !== 'undefined') {
                var promiseObj = repository.getNewsItem(id);
                promiseObj.then(function (value) {
                    $scope.news = value;
                });
            }
        }
        return newsItemCtrl;
    }());
    testApp.newsItemCtrl = newsItemCtrl;
})(testApp || (testApp = {}));
//# sourceMappingURL=newsItemCtrl.js.map