///<reference path="../service/TestAppRepository.ts"/>


namespace testApp{
  export class registrationCtrl{
    private _$scope;
    private _accoutManager;

    constructor($scope, accoutManager){
      this._$scope = $scope;
      this._accoutManager = accoutManager;
      $scope.vm = this;
      
      $scope.email = '';
      $scope.password = '';

      $scope.messageVisible = false;
      $scope.formVisible = true;
    }
    
    submit(){
      let promiseObj = this._accoutManager.registration(this._$scope.email, this._$scope.password);
      let temp = this._$scope;

      promiseObj.then(function(value) {
        temp.messageVisible = true;
        temp.formVisible = false;
      });
    }
  }
}