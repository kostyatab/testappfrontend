///<reference path="../abstract/ITestAppRepository.ts"/>
var testApp;
(function (testApp) {
    var createNewsCtrl = (function () {
        function createNewsCtrl($scope, repository) {
            this._$scope = $scope;
            this._repository = repository;
            $scope.vm = this;
            $scope.visibleForm = true;
            $scope.visibleText = false;
        }
        createNewsCtrl.prototype.submit = function () {
            var news = {
                Id: 0,
                Autor: this._$scope.Autor,
                Title: this._$scope.Title,
                Content: this._$scope.Content,
                Date: new Date()
            };
            var than = this;
            var promiseObj = this._repository.createNews(news);
            promiseObj.then(function (value) {
                than._$scope.visibleForm = false;
                than._$scope.visibleText = true;
            });
        };
        return createNewsCtrl;
    }());
    testApp.createNewsCtrl = createNewsCtrl;
})(testApp || (testApp = {}));
//# sourceMappingURL=createNewsCtrl.js.map