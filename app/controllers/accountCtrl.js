var testApp;
(function (testApp) {
    var accountCtrl = (function () {
        function accountCtrl($scope, accoutManager, $rootScope) {
            this._$scope = $scope;
            $scope.vm = this;
            this._accoutManager = accoutManager;
            $rootScope.$watch('userInfo', function () { $scope.userInfo = $rootScope.userInfo; });
        }
        accountCtrl.prototype.logout = function () {
            this._accoutManager.logout();
        };
        return accountCtrl;
    }());
    testApp.accountCtrl = accountCtrl;
})(testApp || (testApp = {}));
//# sourceMappingURL=accountCtrl.js.map